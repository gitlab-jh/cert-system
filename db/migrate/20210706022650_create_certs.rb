class CreateCerts < ActiveRecord::Migration[6.1]
  def change
    create_table :certs do |t|
      t.string :cert_id
      t.string :first_name
      t.string :last_name
      t.integer :cert_type
      t.integer :status
      t.datetime :expire_date
      t.string :encrypted_cert_id

      t.timestamps
    end
  end
end
