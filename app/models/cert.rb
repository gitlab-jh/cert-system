require 'securerandom'

class Cert < ApplicationRecord

  after_save :set_default_cert_id

  enum status: {
    'verified': 1,
    'unverified': 2
  }

  enum cert_type: {
    'JiHu GitLab Associate': 1,
    'JiHu GitLab CI/CD Specialist': 2,
    'JiHu GitLab PM Specialist': 3,
    'JiHu GitLab Security Specialist': 4,
    'JiHu GitLab System Admin Specialist': 5
  }

  def set_default_cert_id
    update!({
      cert_id: "JH-#{SecureRandom.hex(5)}",
      encrypted_cert_id: SecureRandom.uuid
    }) if !cert_id.present? || !encrypted_cert_id.present?
  end
end
