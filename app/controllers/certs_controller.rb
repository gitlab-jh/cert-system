class CertsController < ApplicationController

   skip_before_action :verify_authenticity_token

  # post /certs/cert_query {cert_id: 'JH-xxxxx'}
  def cert_query
    @cert = Cert.where({ cert_id: params[:cert_id] }).last
    render json: { encrypted_cert_id: @cert.encrypted_cert_id }
  end

  # post /certs/cert_info {encrypted_cert_id: 'xxxxxx-xxxxx'}
  def cert_info
    @cert = Cert.where({ encrypted_cert_id: params[:encrypted_cert_id] }).last
    render json: @cert
  end
end
