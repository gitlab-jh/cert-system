Rails.application.routes.draw do

  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :certs do
    collection do
      post :cert_query
      post :cert_info
    end
  end
  get '/', to: redirect('/admin')
end
